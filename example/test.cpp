
#include "MyClass.hpp"
#include <iostream>

int main(int argc, char** argv)
{
    std::cout << "Hello from main!" << std::endl;

    MyClass::print_hello();

    return 0;
}

